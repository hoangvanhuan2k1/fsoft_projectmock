import AdminInput from "@/components/inputs/adminInputs";
import axios from "axios";
import { Form, Formik, FormikHelpers } from "formik";
import React, { useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import styles from "./styles.module.scss";

interface Category {
  id: string;
  name: string;
}

interface Props {
  setCategories: (categories: Category[]) => void;
}

interface FormValues {
  name: string;
}

interface ApiResponse {
  categories: Category[];
  message: string;
}

export default function Create({ setCategories }: Props) {
  const [name, setName] = useState<string>("");

  const validate = Yup.object({
    name: Yup.string()
      .required("Category name is required.")
      .min(2, "Category name must be between 2 and 30 characters.")
      .max(30, "Category name must be between 2 and 30 characters."),
  });

  const submitHandler = async (values: FormValues, actions: FormikHelpers<FormValues>) => {
    try {
      const { data } = await axios.post<ApiResponse>("/api/admin/category", { name: values.name });
      setCategories(data.categories);
      setName("");
      toast.success(data.message);
    } catch (error: any) {
      toast.error(error.response?.data?.message || "An error occurred.");
    } finally {
      actions.setSubmitting(false);
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={{ name }}
      validationSchema={validate}
      onSubmit={submitHandler}
    >
      {(formik) => (
        <Form>
          <div className={styles.header}>Create a Category</div>
          <AdminInput
            type="text"
            label="Name"
            name="name"
            placeholder="Category name"
            onChange={(e) => setName(e.target.value)}
          />
          <div className={styles.btnWrap}>
            <button type="submit" className={styles.btn} disabled={formik.isSubmitting}>
              <span>Add Category</span>
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
