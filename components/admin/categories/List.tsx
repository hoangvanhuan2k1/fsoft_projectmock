import React from "react";
import ListItem from "./ListItem";
import styles from "./styles.module.scss";

interface Category {
  _id: string;
  name: string;
}

interface Props {
  categories: Category[];
  setCategories: (categories: Category[]) => void;
}

export default function List({ categories, setCategories }: Props) {
  return (
    <ul className={styles.list}>
      {categories.map((category) => (
        <ListItem
          category={category}
          key={category._id}
          setCategories={setCategories}
        />
      ))}
    </ul>
  );
}
