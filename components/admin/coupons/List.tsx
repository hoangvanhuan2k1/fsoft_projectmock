import React from "react";
import ListItem from "./ListItem";
import styles from "./styles.module.scss";

interface Coupon {
  _id: string;
  name: string;
  discount: number;
  startDate: Date;
  endDate: Date;
}

interface Props {
  coupons: Coupon[];
  setCoupons: (coupons: Coupon[]) => void;
}

export default function List({ coupons, setCoupons }: Props) {
  return (
    <ul className={styles.list}>
      {coupons.map((coupon) => (
        <ListItem coupon={coupon} key={coupon._id} setCoupons={setCoupons} />
      ))}
    </ul>
  );
}
