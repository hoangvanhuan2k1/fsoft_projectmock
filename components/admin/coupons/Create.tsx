import { Form, Formik, FormikHelpers } from "formik";
import { useState } from "react";
import styles from "./styles.module.scss";
import * as Yup from "yup";
import { toast } from "react-toastify";
import axios from "axios";
import { TextField } from "@material-ui/core";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import AdminInput from "@/components/inputs/adminInputs";

interface Coupon {
  _id: string;
  name: string;
  discount: number;
  startDate: Date;
  endDate: Date;
}

interface Props {
  setCoupons: (coupons: Coupon[]) => void;
}

interface FormValues {
  name: string;
  discount: number;
}

export default function Create({ setCoupons }: Props) {
  const [name, setName] = useState<string>("");
  const [discount, setDiscount] = useState<number>(0);
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [endDate, setEndDate] = useState<Date>(tomorrow);

  const handleStartDate = (newValue: Date | null) => {
    if (newValue) setStartDate(newValue);
  };

  const handleEndDate = (newValue: Date | null) => {
    if (newValue) setEndDate(newValue);
  };

  const validate = Yup.object({
    name: Yup.string()
      .required("Coupon name is required.")
      .min(2, "Coupon name must be between 2 and 30 characters.")
      .max(30, "Coupon name must be between 2 and 30 characters."),
    discount: Yup.number()
      .required("Discount is required.")
      .min(1, "Discount must be at least 1%")
      .max(99, "Discount must be 99% or less"),
  });

  const submitHandler = async (values: FormValues, actions: FormikHelpers<FormValues>) => {
    try {
      if (startDate.toString() === endDate.toString()) {
        toast.error("You can't pick the same Dates.");
        return;
      }
      if (endDate.getTime() < startDate.getTime()) {
        toast.error("Start Date cannot be more than the end date.");
        return;
      }
      const { data } = await axios.post("/api/admin/coupon", {
        coupon: values.name,
        discount: values.discount,
        startDate,
        endDate,
      });
      setCoupons(data.coupons);
      setName("");
      setDiscount(0);
      setStartDate(new Date());
      setEndDate(tomorrow);
      toast.success(data.message);
      actions.resetForm();
    } catch (error: any) {
      toast.error(error.response?.data?.message || "An error occurred.");
    } finally {
      actions.setSubmitting(false);
    }
  };

  return (
    <Formik
      enableReinitialize
      initialValues={{ name, discount }}
      validationSchema={validate}
      onSubmit={submitHandler}
    >
      {(formik) => (
        <Form>
          <div className={styles.header}>Create a Coupon</div>
          <AdminInput
            type="text"
            label="Name"
            name="name"
            placeholder="Coupon name"
            value={formik.values.name}
            onChange={(e) => {
              formik.handleChange(e);
              setName(e.target.value);
            }}
            onBlur={formik.handleBlur}
          />
          {formik.touched.name && formik.errors.name ? (
            <div className={styles.error}>{formik.errors.name}</div>
          ) : null}
          <AdminInput
            type="number"
            label="Discount"
            name="discount"
            placeholder="Discount"
            value={formik.values.discount}
            onChange={(e) => {
              formik.handleChange(e);
              setDiscount(Number(e.target.value));
            }}
            onBlur={formik.handleBlur}
          />
          {formik.touched.discount && formik.errors.discount ? (
            <div className={styles.error}>{formik.errors.discount}</div>
          ) : null}
          <div className={styles.date_picker}>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DesktopDatePicker
                label="Start Date"
                inputFormat="MM/dd/yyyy"
                value={startDate}
                onChange={handleStartDate}
                renderInput={(params) => <TextField {...params} />}
                minDate={new Date()}
              />
              <DesktopDatePicker
                label="End Date"
                inputFormat="MM/dd/yyyy"
                value={endDate}
                onChange={handleEndDate}
                renderInput={(params) => <TextField {...params} />}
                minDate={tomorrow}
              />
            </LocalizationProvider>
          </div>
          <div className={styles.btnWrap}>
            <button type="submit" className={styles.btn} disabled={formik.isSubmitting}>
              <span>Add Coupon</span>
            </button>
          </div>
        </Form>
      )}
    </Formik>
  );
}
