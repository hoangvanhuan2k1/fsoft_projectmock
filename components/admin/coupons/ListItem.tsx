import React, { useRef, useState } from "react";
import styles from "./styles.module.scss";
import axios from "axios";
import { toast } from "react-toastify";
import { TextField } from "@material-ui/core";
import { DesktopDatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { AiFillDelete, AiTwotoneEdit } from "react-icons/ai";

interface Coupon {
  _id: string;
  coupon: string;
  discount: number;
  startDate: Date;
  endDate: Date;
}

interface Props {
  coupon: Coupon;
  setCoupons: (coupons: Coupon[]) => void;
}

export default function ListItem({ coupon, setCoupons }: Props) {
  const [open, setOpen] = useState<boolean>(false);
  const [name, setName] = useState<string>("");
  const [discount, setDiscount] = useState<string>("");
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  const [startDate, setStartDate] = useState<Date>(new Date(coupon.startDate));
  const [endDate, setEndDate] = useState<Date>(new Date(coupon.endDate));

  const handleStartDate = (newValue: Date | null) => {
    if (newValue) setStartDate(newValue);
  };

  const handleEndDate = (newValue: Date | null) => {
    if (newValue) setEndDate(newValue);
  };

  const inputRef = useRef<HTMLInputElement>(null);

  const handleRemove = async (id: string) => {
    try {
      const { data } = await axios.delete("/api/admin/coupon", {
        data: { id },
      });
      setCoupons(data.coupons);
      toast.success(data.message);
    } catch (error: any) {
      toast.error(error.response?.data?.message || "An error occurred.");
    }
  };

  const handleUpdate = async (id: string) => {
    try {
      const { data } = await axios.put("/api/admin/coupon", {
        id,
        coupon: name || coupon.coupon,
        discount: discount || coupon.discount,
        startDate,
        endDate,
      });
      setCoupons(data.coupons);
      setOpen(false);
      toast.success(data.message);
    } catch (error: any) {
      toast.error(error.response?.data?.message || "An error occurred.");
    }
  };

  return (
    <li className={styles.list__item}>
      <input
        className={open ? styles.open : ""}
        type="text"
        value={name || coupon.coupon}
        onChange={(e) => setName(e.target.value)}
        disabled={!open}
        ref={inputRef}
      />
      {open && (
        <div className={styles.list__item_expand}>
          <input
            className={open ? styles.open : ""}
            type="text"
            value={discount || coupon.discount.toString()}
            onChange={(e) => setDiscount(e.target.value)}
            disabled={!open}
          />
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DesktopDatePicker
              label="Start Date"
              inputFormat="MM/dd/yyyy"
              value={startDate}
              onChange={handleStartDate}
              renderInput={(params) => <TextField {...params} />}
              minDate={new Date()}
            />
            <DesktopDatePicker
              label="End Date"
              inputFormat="MM/dd/yyyy"
              value={endDate}
              onChange={handleEndDate}
              renderInput={(params) => <TextField {...params} />}
              minDate={tomorrow}
            />
          </LocalizationProvider>
          <button className={styles.btn} onClick={() => handleUpdate(coupon._id)}>
            Save
          </button>
          <button
            className={styles.btn}
            onClick={() => {
              setOpen(false);
              setName("");
              setDiscount("");
              setStartDate(new Date(coupon.startDate));
              setEndDate(new Date(coupon.endDate));
            }}
          >
            Cancel
          </button>
        </div>
      )}
      <div className={styles.list__item_actions}>
        {!open && (
          <AiTwotoneEdit
            onClick={() => {
              setOpen((prev) => !prev);
              inputRef.current?.focus();
            }}
          />
        )}
        <AiFillDelete onClick={() => handleRemove(coupon._id)} />
      </div>
    </li>
  );
}
